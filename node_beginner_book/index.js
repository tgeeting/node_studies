//imports server creation from local server.js
var server = require("./server.js");
var router = require("./router.js");
var requestHandlers = require("./requestHandlers.js");

//handle object contains url request extensions and appropriate
//  request handler functions
var handle = {
    "/": requestHandlers.start,
    "/start": requestHandlers.start,
    "/upload": requestHandlers.upload,
    "/show": requestHandlers.show
};

server.start(router.route, handle);