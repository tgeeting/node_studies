//contains createServer function
var http = require("http");
//contains parse function
var url = require("url");

//used to export server creation to main file
function start(route, handle) {
    
    //named function callback for http.createServer
    function onRequest(request, response) {
        var pathname = url.parse(request.url).pathname;     //parses the requested url pathname
        console.log("Request for " + pathname + " received.");    //logs only when request has been made
        route(handle, pathname, response, request);
    }

http.createServer(onRequest).listen(process.env.PORT);
console.log("Server has started."); //console logs regardless of request
}

exports.start = start;