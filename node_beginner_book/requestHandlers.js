//supplies the parse constructor where the text attribute is used
var querystring = require("querystring");
var fs = require("fs");
var formidable = require("formidable");

//request handler for / & /start request
function start(response) {
    console.log("Request handler 'start' was called.");
    
    //typically would not have view content in the request handler
    // or perhaps would leave it for client side framework
    var body = '<html>'+
    '<head>'+
        '<meta http-equiv="Content-Type" content="text/html; '+
        'charset=UTF-8"/>'+
    '</head>'+
    '<body>'+
        '<form action="/upload"  enctype= "multipart/formmethod="post">'+
            '<input type="submit" value="Upload file" />'+
        '</form>'+
    '</body>'+
    '</html>';
    
    response.writeHead(200, {"Content-Type": "text/html"});
    response.write(body);
    response.end();
}

//request handler for /upload request
function upload(response, request) {
    console.log("Request handler 'upload' was called.");
    
    var form = new formidable.IncomingForm();
    console.log("about to parse");
    form.parse(request, function(error, fields, files) {
        console.log("parsing done");
        
        //Commented out as Cloud9 IDE doesn't allow for local uploads to webserver
        /*
        fs.rename(files.upload.path, "/tmp/test.png", function(error) {
            if (error) {
                fs.unlink("/tmp/test.png");
                fs.rename(files.upload.path, "/tmp/test.png"); 
            }
        });
        */
        
        response.writeHead(200, {"Content-Type": "text/html"});
        response.write("received image <br/>");
        response.write("<img src='/show' />");
        response.end();
    });
}

//shows the image saved as test.png in the tmp file
function show(response) {
    console.log("Request handler 'show' was called.");
    response.writeHead(200, {"Content-Type": "image/png"});
    fs.createReadStream("tmp/test.png").pipe(response);
}

exports.start = start;
exports.upload = upload;
exports.show = show;