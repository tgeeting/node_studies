function route(handle, pathname, response, request) {
    console.log("About to route a request for " + pathname); //logs the pathname request
    
    //if a handler for the pathname exists, execute the handler
    if (typeof handle[pathname] === 'function') {
        return handle[pathname](response, request);
    }
    //if no handler is found console log this message
    else {
        console.log("No request handler found for " + pathname);
        response.writeHead(404, {"Content-Type": "text/plain"});
        response.write("404 Not found");
        response.end();
    }
}

exports.route = route;