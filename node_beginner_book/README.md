#Beginner Node Tutorial#
##A tutorial app as explained through "The Node Beginner Book: A comprehensive Node.js tutorial" by Manuel Kiessling##

The app allows a user to upload a file which can then be viewed on a webpage.

Run with:
```
#!javascript
node index.js

```