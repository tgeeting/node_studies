//MAIN FILE//
/***
**Configure application
**Connect to database
**Create Mongoose Models
**Define routes for RESTful API
**Define routes for Angular Frontend
**Set app to listen on port
***/

//MUST RUN ./mongod before running server.js otherwise will not connect to DB


/**
 * Module Import
 */
var express         = require('express'); //back-end framework for node.js
var app             = express();
var mongoose        = require('mongoose'); //module to configure use for mongoDB with node.js
var morgan          = require('morgan'); //logs requests to the console for use with express
var bodyParser      = require('body-parser'); //pull info from html POST request for use with express
var methodOverride  = require('method-override'); //simulate DELETE and PUT request for use with express
var database        = require('./config/database.js');

/**
 * Sever configuration
 */
mongoose.connect(database.url);                             //connects to mongoDB locally on cloud9

app.use(express.static(__dirname + '/public'));             //sets static file location to /public so it is / for users
app.use(morgan('dev'));                                     //logs requests to consoles
app.use(bodyParser.urlencoded({'extended':'true'}));        //parse application/x-www/form-urlencoded
app.use(bodyParser.json());                                 //parse application/json
app.use(bodyParser.json({type:'application/vnd.api+json'}));//parse application/vnd.api+json as json
app.use(methodOverride());


/**
 * Console logs database error
 */
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function callback () {
   console.log("connected");
 });


/**
 * Routes the requests
 */
require('./app/routes.js')(app);


/**
 * Listen for requests on cloud9 port
 */
app.listen(process.env.PORT);
console.log("App listening on cloud9 port");

