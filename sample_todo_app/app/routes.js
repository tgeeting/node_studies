// app/routes.js

var Todo = require('./models/todo.js');

module.exports = function(app) {
    
    //often used Todo DB query function
    function todoJSONResponse(res) {
        Todo.find(function(err, todos) {
            if(err) { res.send(err) }
            res.json(todos);
            console.log(todos);
        });
    }

    //retrieves todos from DB [GET] @ url: /api/todos
    app.get('/api/todos', function(req, res) {
        todoJSONResponse(res);
    });

    //posts todos and enters them into DB [POST] @ url: /api/todos
    app.post('/api/todos', function(req, res) {
        Todo.create({
            text: req.body.text,
            dont: false,
        }, function(err, todo) {
            if(err) { res.send(err); }
            todoJSONResponse(res);
        });
    });

    //removes todos from the DB [DELETE] @ url: /api/todos/:todo_id
    app.delete('/api/todos/:todo_id', function(req, res) {
        Todo.remove({
            _id: req.params.todo_id
        }, function(err, todo) {
            if (err) { res.send(err); }
            todoJSONResponse(res);
        });
    });

    //retrives the main page html to show [GET] @ url: /
    app.get('*', function(req, res) {
        res.sendfile('./public/index.html');
    });
};